"""Common pronouns used by things"""

class Inanimate(object):
    """A set of pronouns for inanimate objects."""
    Subject_Pronoun = "It"
    Object_Pronoun = "It"
    Possessive_adj = "Its"
    Possessive_noun = "Its"
    Reflexive_Pronoun = "Itself"

class Male(object):
    """A set of pronouns for male gendered objects."""
    Subject_Pronoun = "He"
    Object_Pronoun = "Him"
    Possessive_adj = "His"
    Possessive_noun = "His"
    Reflexive_Pronoun = "Himself"

class Female(object):
    """A set of pronouns for female gendered objects."""
    Subject_Pronoun = "She"
    Object_Pronoun = "Her"
    Possessive_adj = "Her"
    Possessive_noun = "Hers"
    Reflexive_Pronoun = "Herself"

class Genderless(object):
    """A set of pronouns for ambigiously gendered objects."""
    Subject_Pronoun = "They"
    Object_Pronoun = "Them"
    Possessive_adj = "Their"
    Possessive_noun = "Theirs"
    Reflexive_Pronoun = "Themself"

class Thing(object):
    """The primitive form of any physical object
    existing in the game world.
    Characters, Items, Rooms, are all subclasses of this.
    This class needs a better name, Body? Game_Object?"""
    
    def __init__(self,
                 name='Something',
                 gender= Inanimate,
                 messages={'desc':"Some thing"},
                 parent=None,
                 children =[]):
        #sets the name and default variables.
        self.name = name
        self.gender = gender
        self.messages = messages
        self.parent = parent
        self.children = children

    def __str__(self):
        #returns the name rather than a confusing memory address
        return self.name
    
    def __repr__(self):
        #returns the name rather than a confusing memory address
        return self.name

    def set_message(self, message, string):
        #sets a message
        self.messages[message] = string
        
    def show(self, message):
        #gets the message
        return self.messages[message]
    
    def adopt(self, child):
        #adds a thing to the list of children
        child.parent = self
        self.children.append(child)
        
    def disown(self, child):
        #removes a thing from the list of children.
        child.parent = None
        self.children.remove(child)

    def test(self):
        #List pronouns
        print self.s
        print self.S
        print self.o
        print self.O
        print self.p
        print self.P
        print self.q
        print self.Q
        print self.r
        print self.R

    @property
    def s(self):
        #lowercase subject pronoun
        return (self.gender.Subject_Pronoun).lower()
    @property
    def S(self):
        #uppercase subject pronoun
        return self.gender.Subject_Pronoun
    @property
    def o(self):
        #lowercase object pronoun
        return (self.gender.Object_Pronoun).lower()
    @property
    def O(self):
        #uppercase object pronoun
        return self.gender.Object_Pronoun
    @property
    def p(self):
        #lowercase posessive adjective
        return (self.gender.Possessive_adj).lower()
    @property
    def P(self):
        #uppercase possessive adjective
        return self.gender.Possessive_adj
    @property
    def q(self):
        #lowercase posessive noun
        return (self.gender.Possessive_noun).lower()
    @property
    def Q(self):
        #uppercase possessive noun
        return self.gender.Possessive_noun
    @property
    def r(self):
        #lowercase reflexive pronoun
        return (self.gender.Reflexive_Pronoun).lower()
    @property
    def R(self):
        #uppercase reflexive pronoun
        return self.gender.Reflexive_Pronoun